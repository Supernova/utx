#!/bin/bash
i=1
sudo chmod 777 /sys/devices/platform/leds/leds/pinephone\:red\:user/brightness
sudo chmod 777 /sys/devices/platform/leds/leds/pinephone\:green\:user/brightness
sudo chmod 777 /sys/devices/platform/leds/leds/pinephone\:blue\:user/brightness
while [ $i -le 30 ]
do
echo 1 > /sys/devices/platform/leds/leds/pinephone\:red\:user/brightness
sleep .2
echo 0 > /sys/devices/platform/leds/leds/pinephone\:red\:user/brightness
echo 1 > /sys/devices/platform/leds/leds/pinephone\:green\:user/brightness
sleep .2
echo 0 > /sys/devices/platform/leds/leds/pinephone\:green\:user/brightness
echo 1 > /sys/devices/platform/leds/leds/pinephone\:blue\:user/brightness
sleep .2
echo 0 > /sys/devices/platform/leds/leds/pinephone\:blue\:user/brightness
i=$(( $i + 1 ))
done
