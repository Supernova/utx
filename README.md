# utx

### Ubuntu Touch scripts for the PinePhone

dirks.sh - show menu for dirkjanbuter's [pinephone-commands](https://github.com/dirkjanbuter/pinephone-commands)

disssh.sh - Disable and stop the ssh service

enssh.sh - Enable and start the ssh service

ledfun.sh - Blinkenlights? Inspired by [dirkjanbuter](https://github.com/dirkjanbuter/pinephone-commands/blob/master/green-on.sh)

setupufw.sh - Configure the ufw firewall with settings that only allow incoming traffic to port 7576 for ssh

upgrade.sh - system upgrade


Testing folder:

enmodem.sh - start cellular modem

ensound.sh - Enable sound mixer

listmix.sh - list all mixer controls

restoremix.sh - restore saved mixer settings

storemix.sh - store current mixer settings


### About

Some of these adapted from [this post](https://forum.pine64.org/showthread.php?tid=8923)

I have not yet thoroughly tested the scripts in the testing folder.

To get these scripts onto your phone you can clone this repository from the phone's command line:

git clone https://codeberg.org/Supernova/utx.git

