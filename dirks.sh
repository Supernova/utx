#!/bin/bash
gitdir=~/dirks
if [ ! -d $gitdir ]
then
  git clone https://github.com/dirkjanbuter/pinephone-commands.git $gitdir
  cd $gitdir
else
  cd $gitdir
  git pull
fi
./menu
